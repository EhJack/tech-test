import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { mount, configure } from 'enzyme';
import { MemoryRouter } from 'react-router';
import { BrowserRouter } from "react-router-dom";
import Adapter from 'enzyme-adapter-react-16';

import List from './App/views/List';
import Login from './App/views/Login';
import NotFoundPage from './App/views/NotFoundPage';
import App from './App/App';

configure({ adapter: new Adapter() });

jest.mock('axios');

describe('App', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(
          <BrowserRouter>
            <App />
          </BrowserRouter>,
          div
      );
      ReactDOM.unmountComponentAtNode(div);
    });
});

describe('Router', () => {
    test('invalid path should redirect to 404', () => {
      const wrapper = mount(
        <MemoryRouter initialEntries={[ '/invalid' ]}>
          <App/>
        </MemoryRouter>
      );
      expect(wrapper.find(Login)).toHaveLength(0);
      expect(wrapper.find(NotFoundPage)).toHaveLength(1);
    });

    test('valid path should not redirect to 404', () => {
      const wrapper = mount(
        <MemoryRouter initialEntries={[ '/' ]}>
          <App/>
        </MemoryRouter>
      );
      expect(wrapper.find(Login)).toHaveLength(1);
      expect(wrapper.find(NotFoundPage)).toHaveLength(0);
    });
});

describe('Login', () => {
    test('returns token if credentials are correct', () => {
        axios.post.mockImplementation(() => Promise.resolve({
            success: true,
            message: 'Authentication successful!',
            token: 'thisisasecrettoken',
        }));
    });

    test('returns 403 if incorrect credentials', () => {
        axios.post.mockImplementation(() => Promise.reject({
            success: false,
            message: 'Incorrect username or password',
        }));
    })
})
