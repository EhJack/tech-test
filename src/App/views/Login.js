import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import withContext from '../contextHOC';

import './Login.css';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
          username: "",
          password: "",
          error: null,
        };
    }

    componentDidMount() {
        const { isAuth } = this.props.auth;
        if (isAuth) {
            this.props.history.push('/');
        }
    }

    validateForm = () => {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = await this.doLogin();
        if (data.success) {
            this.setState({ error: null });
            this.props.auth.login(data.token);
            this.props.history.push('/');
        } else {
            this.setState({ error: 'Incorrect username or password!' });
        }
    }

    doLogin = async () => {
        try {
            const { data } = await axios({
                method: 'post',
                url: 'http://localhost:5000/api/login',
                data: { username: this.state.username, password: this.state.password },
                headers: {
                    "Authorization": `Bearer ${process.env.REACT_APP_AUTH_TOKEN}`
                }
            });

            return data;
        } catch (e) {
            return e;
        }
    }

    render() {
        return (
            <div className="App">
                <h1>Login</h1>

                <div className="Login">
                    {this.state.error &&
                        <div className="error">
                            {this.state.error}
                        </div>
                    }

                    <form onSubmit={this.handleSubmit}>

                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text" id="username" onChange={this.handleChange} value={this.state.username} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" onChange={this.handleChange} value={this.state.password} />
                        </div>

                        <button disabled={!this.validateForm()}>Login</button>

                    </form>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    auth: PropTypes.shape({
        isAuth: PropTypes.bool.isRequired
    }),
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired,
}

export default withContext(Login);
