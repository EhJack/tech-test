import React, { Component } from 'react';

class NotFoundPage extends Component {
  render() {
    return (
    <div className="App">
      <h1>Page Not Found!</h1>
      <p>The page you are looking for was not found!</p>
    </div>
    );
  }
}
export default NotFoundPage;
