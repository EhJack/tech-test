import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withContext from '../contextHOC';

import './List.css';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
    }

    componentDidMount() {
        this.getList();
    }

    logout = (e) => {
        e.preventDefault();
        this.props.auth.logout();
    }

    getList = async () => {
        const quotes = await fetch(`https://breaking-bad-quotes.herokuapp.com/v1/quotes/10`);
        const json = await quotes.json();

        this.setState({ list: json });
    }

    render() {
        const { list } = this.state;
        return (
            <div className="App">
                <h1>Breaking Bad Quotes <button className="logout" onClick={this.logout}>Logout</button></h1>

                <section className="quotes">
                    <ul className="list">
                        {list.length ? (
                            <>
                                {list.map((item, key) => {
                                    return(
                                        <li key={key}>
                                            <blockquote>
                                                {item.quote}
                                                <footer>
                                                    <cite>
                                                        {item.author}
                                                    </cite>
                                                </footer>
                                            </blockquote>
                                        </li>
                                    );
                                })}
                            </>
                        ) : (
                            <li>
                                <h2>No List Items Found</h2>
                            </li>
                        )}
                    </ul>
                </section>
          </div>
        );
    }
}

List.propTypes = {
    auth: PropTypes.shape({
        logout: PropTypes.func.isRequired
    })
}

export default withContext(List);
