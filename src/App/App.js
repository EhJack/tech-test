import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { AuthProvider } from './AuthContext'
import PrivateRoute from './PrivateRoute';
import Login from './views/Login';
import List from './views/List';
import NotFoundPage from './views/NotFoundPage';

const App = () => (
    <>
        <AuthProvider>
            <Switch>
                <PrivateRoute exact path="/" component={List} />
                <Route path="/login" component={Login} />
                <Route component={NotFoundPage} />
            </Switch>
        </AuthProvider>
    </>
);

export default App;
