import React from 'react';
import { AuthContext } from './AuthContext';

const withContext = (Component) => {
    return (props) => <AuthContext.Consumer>
        {(auth) => <Component {...props} auth={auth} />}
    </AuthContext.Consumer>
}

export default withContext;
