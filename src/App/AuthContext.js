import React from 'react'

const AuthContext = React.createContext({
    isAuth: false,
    token: null,
});

class AuthProvider extends React.Component {
  state = {
      isAuth: false,
      token: null
  }

  componentWillMount() {
      const authToken = sessionStorage.getItem('auth_token');
      if (authToken) {
          this.setState({ isAuth: true, token: authToken });
      }
  }

  logout = () => {
      this.setState({ isAuth: false, token: null });
      sessionStorage.clear();
  }

  login = token => {
      this.setState({ isAuth: true, token });
      sessionStorage.setItem('auth_token', token);
  }

  render() {
    return (
      <AuthContext.Provider value={{
          isAuth: this.state.isAuth,
          token: this.state.token,
          login: this.login,
          logout: this.logout
      }}>
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}

const AuthConsumer = AuthContext.Consumer

export { AuthContext, AuthProvider, AuthConsumer }
