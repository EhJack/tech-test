Tech Test
===================

Tech Test for Vidatec.

Install node modules for client and server:
```
$ yarn install
$ cd server
$ yarn install
```

Rename ```.env.example``` to ```.env```

Edit the ```.env``` file in the root directory and paste the following key:

```
REACT_APP_AUTH_TOKEN=XhVMjSzr62
```

Go to the server directory and start the server and go to the root directory to run create react app:
```
$ yarn run start
```

Your browser should automatically launch or you can visit: http://localhost:3000

The fake user credentials are:
* Username: demo
* Password: demo
