const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
let jwt = require('jsonwebtoken');
let config = require('./config');
const middleware = require('./middleware');

class HandlerGenerator {
  login (req, res) {
    let username = req.body.username;
    let password = req.body.password;

    let mockedUsername = 'demo';
    let mockedPassword = 'demo';

    if (username && password) {
      if (username === mockedUsername && password === mockedPassword) {
        let token = jwt.sign({username: username},
          config.secret,
          { expiresIn: '24h' // expires in 24 hours
          }
        );
        // return the JWT token for the future API calls
        res.json({
          success: true,
          message: 'Authentication successful!',
          token: token
        });
        return;
      } else {
        res.status(403).json({
          success: false,
          message: 'Incorrect username or password'
        });
        return;
      }
    } else {
      res.status(400).json({
        success: false,
        message: 'Authentication failed! Please check the request'
      });
      return;
    }
  }
  list (req, res) {
    res.json({
      success: true,
      message: 'List data'
    });
    return;
  }
}

const app = express();
const handlers = new HandlerGenerator();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/api/login', handlers.login);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server is running on PORT ${PORT}`);
});
